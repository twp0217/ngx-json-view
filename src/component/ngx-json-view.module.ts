import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxJsonViewComponent } from './ngx-json-view.component';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [
    NgxJsonViewComponent
  ],
  declarations: [
    NgxJsonViewComponent
  ]
})
export class NgxJsonViewModule { }
